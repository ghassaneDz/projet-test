FROM ubuntu:18.04

MAINTAINER Kevin Allioli <kevin@linit.io>

# Installation des prérequis
RUN apt-get update && apt-get install -y wget sudo supervisor git redis && \
    mkdir -p /var/log/supervisor && \
    mkdir -p /etc/supervisor/conf.d

ADD supervisor.conf /etc/supervisor.conf

# Installation de NodeJS
RUN wget -qO- https://deb.nodesource.com/setup_10.x | sudo -E bash - && \
    apt-get install -y nodejs

# Installation de SCRUMBLR 
RUN git clone https://gitlab.com/kallioli/scrumblr.git && \
    cd scrumblr && \
    npm install

WORKDIR /scrumblr
EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["supervisord", "-c", "/etc/supervisor.conf"]
